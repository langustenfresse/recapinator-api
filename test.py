import unittest
import app

class RecapinatorTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()

    def test_rootIsHelloWorld(self):
        root = self.app.get('/')
        assert root.data == b"Hello World!"

if __name__ == "__main__":
    unittest.main()